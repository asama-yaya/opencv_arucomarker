#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
#include <opencv2/aruco.hpp>		// aruco marker
#include <opencv2/aruco/charuco.hpp>// for aruco chess board
// MyHeader-------------------------------------------------------------------------------------
#include "camera_class.h"


using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

namespace{
	const int ARUCO_SingleMarker = 0;
	const int ARUCO_Board = 1;
	const int ARUCO_ChessBoard = 2;
	const int ARUCO_Diamond = 3;
}

class TMyAruco{

public:
	TMyAruco(){}
	~TMyAruco(){}

	bool Initialize();
	void MainLoop();

	void GetMatrix(float *_m, int _id);
	cv::Mat GetImage();

private:

	// camera
	TWebCamera webCamera;
	int camera_width, camera_height;
	cv::Mat intrinsic, distortion;
	cv::Mat MapX, MapY;

	// image
	cv::Mat captureImage;
	cv::Mat undistImage;

	//----------------------------------------------------------------------
	// marker
	//----------------------------------------------------------------------

	// marker type
	int markerType = ARUCO_SingleMarker;

	// single marker
	cv::Ptr<cv::aruco::Dictionary> markerDictionary;
	double markerSingleSize = 0.132; // marker size [m]
	vector<cv::Vec3d> rvecsSingle, tvecsSingle;

	// board
	cv::Ptr<cv::aruco::GridBoard> markerBoard;
	cv::Point2d markerBoardSize = cv::Point2d(0.025, 0.007);	// x:marker size, y:distance between marker [m]
	cv::Vec3d rvecBoard, tvecBoard;

	// chess
	cv::Ptr<cv::aruco::CharucoBoard> markerChess;
	cv::Point2d markerChessSize = cv::Point2d(0.031, 0.0155);	// x:square size, y:marker size [m]
	vector<cv::Point2f> cornersChess;
	vector<int> idsChess;

	// diamond
	cv::Point2d markerDiamondSize = cv::Point2d(0.053, 0.0315);	// x:square size, y:marker size [m]
	vector<cv::Vec3d> rvecsDiamond, tvecsDiamond;

};

