#include "MyAruco.h"

// 初期化
bool TMyAruco::Initialize(){

	// marker draw
	if (true){
		cout << "draw marker -> ";
		cv::Mat markerImage;

		// single marker
		if (markerType == ARUCO_SingleMarker){
			cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
			// dict, ID, size, image, bit
			cv::aruco::drawMarker(dictionary, 23, 200, markerImage, 1);
			cv::imwrite("data\\marker.png", markerImage);
		}
		// board
		else if (markerType == ARUCO_Board){
			cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
			// X, Y, length, separation(aida), dictionary 
			cv::Ptr<cv::aruco::GridBoard> board = cv::aruco::GridBoard::create(5, 7, 0.04, 0.01, dictionary);
			board->draw(cv::Size(600, 500), markerImage, 10, 1);
			cv::imwrite("data\\board.png", markerImage);
		}		
		// chessboard
		else if (markerType == ARUCO_ChessBoard){
			cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
			cv::Ptr<cv::aruco::CharucoBoard> board = cv::aruco::CharucoBoard::create(5, 7, 0.04, 0.02, dictionary);
			board->draw(cv::Size(600, 500), markerImage, 10, 1);
			cv::imwrite("data\\chess.png", markerImage);
		}
		// diamond
		else if (markerType == ARUCO_Diamond){
			cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
			cv::aruco::drawCharucoDiamond(dictionary, cv::Vec4i(45, 68, 28, 74), 200, 120, markerImage);
			cv::imwrite("data\\diamond.png", markerImage);
			cv::aruco::drawCharucoDiamond(dictionary, cv::Vec4i(15, 28, 26, 34), 200, 120, markerImage);
			cv::imwrite("data\\diamond(2).png", markerImage);
		}

		// 表示
		cv::imshow("marker", markerImage);
		cout << "おしり" << endl;
		char key = cv::waitKey(0);
		cv::destroyAllWindows();
		if (key == 'q') return true;
	}

	// aruco marker init (位置推定のための準備)
	if (markerType == ARUCO_SingleMarker){
		markerDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
	}
	else if (markerType == ARUCO_Board){
		markerDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
		markerBoard = cv::aruco::GridBoard::create(5, 7, markerBoardSize.x, markerBoardSize.y, markerDictionary);
	}
	else if (markerType == ARUCO_ChessBoard){
		markerDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
		markerChess = cv::aruco::CharucoBoard::create(5, 7, markerChessSize.x, markerChessSize.y, markerDictionary);
	}
	else if (markerType == ARUCO_Diamond){
		markerDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
	}

	//-------------------------------------------------------------------------------------------------
	// camera
	webCamera.init(0);
	captureImage = webCamera.capture().clone();
	camera_width = captureImage.cols;
	camera_height = captureImage.rows;

	// 内部パラメタの読み込み
	std::string  fileName1 = "data\\CAM_PARAM_MAT.xml";
	cv::FileStorage tcvfs(fileName1, cv::FileStorage::READ);
	cv::FileNode param1(tcvfs.fs, NULL);
	cv::read(tcvfs["CameraMatrix"], intrinsic);
	cv::read(tcvfs["DistortCoeffs"], distortion);
	tcvfs.release();
	if (intrinsic.cols != 3){
		cout << "can't read camera parameter: " << fileName1 << endl;
		getchar();
	}

	// 歪補正用のマップ作成
	cv::Mat mapR = cv::Mat::eye(3, 3, CV_64F);
	cv::Mat new_intrinsic = cv::getOptimalNewCameraMatrix(intrinsic, distortion, captureImage.size(), 1);
	cv::initUndistortRectifyMap(intrinsic, distortion, mapR, new_intrinsic, captureImage.size(), CV_32FC1, MapX, MapY);
	intrinsic = new_intrinsic.clone();
	for (int xx = 0; xx < distortion.cols; xx++) distortion.at<double>(0, xx) = 0.0;

	return false;
}

// マーカ認識
void TMyAruco::MainLoop(){

	// capture
	captureImage = webCamera.capture();
	cv::remap(captureImage, undistImage, MapX, MapY, cv::INTER_LINEAR);

	// detect marker
	vector<int> ids;
	vector<vector<cv::Point2f>> corners;
	cv::aruco::detectMarkers(undistImage, markerDictionary, corners, ids);

	// if at least one marker is detected
	if (ids.size() > 0){
		// single marker
		if (markerType == ARUCO_SingleMarker){
			// draw id
			cv::aruco::drawDetectedMarkers(undistImage, corners, ids);

			// pose estimation
			rvecsSingle.clear(); tvecsSingle.clear();
			cv::aruco::estimatePoseSingleMarkers(corners, markerSingleSize, intrinsic, distortion, rvecsSingle, tvecsSingle);

			// draw axis
			for (int ii = 0; ii < ids.size(); ii++)
				cv::aruco::drawAxis(undistImage, intrinsic, distortion, rvecsSingle[ii], tvecsSingle[ii], 0.1);		
		}
		// board
		else if (markerType == ARUCO_Board){
			cv::aruco::drawDetectedMarkers(undistImage, corners, ids);

			int valid = cv::aruco::estimatePoseBoard(corners, ids, markerBoard, intrinsic, distortion, rvecBoard, tvecBoard);
			// if at least one board marker detected
			if (valid > 0)
				cv::aruco::drawAxis(undistImage, intrinsic, distortion, rvecBoard, tvecBoard, 0.1);
		}
		// chessboard
		else if (markerType == ARUCO_ChessBoard){
			cornersChess.clear();
			idsChess.clear();
			cv::aruco::interpolateCornersCharuco(corners, ids, undistImage, markerChess, cornersChess, idsChess);
			cv::aruco::drawDetectedMarkers(undistImage, corners, ids);

			// if at least one charuco corner detected
			if (idsChess.size() > 0)
				cv::aruco::drawDetectedCornersCharuco(undistImage, cornersChess, idsChess, cv::Scalar(0, 0, 255));
		}
		// diamond
		else if (markerType == ARUCO_Diamond){
			// detect diamon diamonds
			vector<cv::Vec4i> idsDiamond;
			vector<vector<cv::Point2f>> cornersDiamond;
			cv::aruco::detectCharucoDiamond(undistImage, corners, ids,
				(markerDiamondSize.x / markerDiamondSize.y), cornersDiamond, idsDiamond);

			cv::aruco::drawDetectedMarkers(undistImage, corners, ids);

			// estimate poses	
			rvecsDiamond.clear();
			tvecsDiamond.clear();
			cv::aruco::estimatePoseSingleMarkers(cornersDiamond, markerDiamondSize.x, 
				intrinsic, distortion, rvecsDiamond, tvecsDiamond);

			// draw axis
			for (unsigned int i = 0; i<rvecsDiamond.size(); i++)
				cv::aruco::drawAxis(undistImage, intrinsic, distortion, rvecsDiamond[i], tvecsDiamond[i], 0.1);
		}
	}


	cv::imshow("image", undistImage);
	char key = cv::waitKey(30);
	if (key == 'q'){
		exit(0);
	}
	else if (key == 'c'){
		cv::imwrite("data\\captureImage.png", captureImage);
		cout << "capture!" << endl;
	}
}

// 推定結果を返す
void TMyAruco::GetMatrix(float *_m, int _id){


}

// 画像を返す
cv::Mat TMyAruco::GetImage(){
	return undistImage;
}